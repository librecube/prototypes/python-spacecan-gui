import time
import random
import spacecan


responder = spacecan.Responder.from_file("config.json")
services = spacecan.Services(responder).from_file("config.json")

set_param = services.parameter_management.set_parameter_value
get_param = services.parameter_management.get_parameter_value

PARAM_COUNTER = 1
PARAM_LED = 2
PARAM_TEMPERATURE = 3

FUNC_COUNTER_RESET = 1
FUNC_LED_ON_OFF = 2


def on_bus_switch():
    print("Bus switched")


def received_scet(coarse_time, fine_time):
    print(f"scet received [{coarse_time, fine_time}]")


def received_utc(day, ms_of_day, sub_ms):
    print(f"utc received [{day, ms_of_day, sub_ms}]")


responder.on_bus_switch = on_bus_switch
responder.received_scet = received_scet
responder.received_utc = received_utc


def perform_function(function_id, arguments):
    if function_id == FUNC_COUNTER_RESET:
        set_param(PARAM_COUNTER, 0)
    elif function_id == FUNC_LED_ON_OFF:
        if arguments[0] == 1:
            set_param(PARAM_LED, 1)
        else:
            set_param(PARAM_LED, 0)


services.function_management.perform_function = perform_function


responder.connect()
responder.start()

try:
    print("Running...")
    while True:
        # do some other stuff
        time.sleep(0.1)

        # update housekeeping data
        set_param(PARAM_COUNTER, get_param(PARAM_COUNTER) + 1)
        set_param(PARAM_TEMPERATURE, 25 + random.random())

except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
print("Stopped")
