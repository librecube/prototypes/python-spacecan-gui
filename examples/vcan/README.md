It uses the virtual CAN network of Linux, which can be started as follows:

```bash
source vcan.sh
```

Then start the responder node. You can check its configuration in the `config.json` file.

```bash
python responder.py
```

In another terminal, start the application:

```bash
spacecan-tester config.json
```

A browser window will open (http://localhost:8080). There you can control and monitor the responder node.
