# Examples

Currently there are two examples provided.

## Local Example

This example is good for getting familiar with SpaceCAN and the use of the
SpaceCAN Test Application. You can run it on your laptop without any additional
hardware. Find it [here](./vcan/README.md).

## Pyboard Example

For this example you need a CAN adaptor and a pyboard as responder node.
Find more information [here](./pyboard/README.md).
