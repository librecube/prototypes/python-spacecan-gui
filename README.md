# LibreCube / Software Ground Support Equipment / SpaceCAN Test Application

This application can be used to test responder nodes connected to the SpaceCAN bus.
Basically, the SpaceCAN Test Application acts as an controller node: it receives
the telemetry from the responder nodes, and it can issue commands to the
responder nodes.

## Installation

Clone the repository and then install via pip:

```bash
python -m venv venv
source venv/bin/activate
pip install .
```

The application can then be started as follows:

```bash
spacecan-tester config.json
```

where `config.json` contains the configuration of the responder node.

## Getting Started

The [examples](./examples/README.md) folder contains a examples on how to use
the SpaceCAN Tester application. Please try it out!

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/xxx/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/xxx

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the GPL license.
See the [LICENSE](./LICENSE.txt) file for details.
