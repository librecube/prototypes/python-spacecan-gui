import argparse
import datetime
import spacecan
from nicegui import ui, app


controller = None


def parse_args():
    parser = argparse.ArgumentParser(description="Start the SpaceCAN tester.")
    parser.add_argument(
        "config_file", help="Path to the configuration file (JSON format)."
    )
    parser.add_argument("--port", default=8080, help="Port of the web app.")
    return parser.parse_args()


args = parse_args()


def setup():
    controller = spacecan.Controller.from_file(args.config_file)
    controller.connect()

    responder = spacecan.Responder.from_file(args.config_file)
    services = spacecan.Services(controller)
    services.from_file(args.config_file, node_id=responder.node_id)

    ui.dark_mode().enable()
    ui.markdown("# SpaceCAN Test Application")
    ui.page_title("SpaceCAN Test Application")

    columns = [
        {
            "label": "Parameter Id",
            "field": "parameter_id",
            "align": "left",
        },
        {
            "label": "Parameter Name",
            "field": "parameter_name",
            "align": "left",
        },
        {
            "label": "Parameter Value",
            "field": "parameter_value",
        },
        {
            "label": "Last Update",
            "field": "last_update",
        },
    ]
    rows = []
    for _, param in services.parameter_management.parameter_pool.items():
        rows.append(
            {
                "parameter_id": str(param.parameter_id),
                "parameter_name": str(param.parameter_name),
                "parameter_value": str(param.value),
            }
        )

    with ui.row().style(
        "width: 100%; justify-content: space-between; align-items: flex-start;"
    ):
        with ui.column().style("flex: 1;"):

            ##################################################################
            ui.markdown("### Basic Actions")
            # ui.separator()
            with ui.row():
                # Switch Bus
                def switch_bus():
                    controller.switch_bus()
                    textbox_current_bus.set_content(
                        f"{controller.get_selected_bus()}"
                    )

                ui.button("Switch Bus", on_click=switch_bus)
                textbox_current_bus = ui.markdown(
                    f"{controller.get_selected_bus()}"
                )

                # Send SCET
                with ui.dialog().props("persistent") as dialog_send_scet, ui.card():

                    def submit():
                        controller.send_scet(
                            int(coarse_time.value), int(fine_time.value)
                        )
                        dialog_send_scet.close()

                    ui.label("Send SCET")
                    coarse_time = ui.number(label="Coarse Time", value=0, format="%1d")
                    fine_time = ui.number(label="Fine Time", value=0, format="%1d")
                    with ui.row():
                        ui.button("Cancel", on_click=dialog_send_scet.close)
                        ui.button("Submit", on_click=submit)
                ui.button("Send SCET", on_click=dialog_send_scet.open)

                # Send UTC
                with ui.dialog().props("persistent") as dialog_send_utc, ui.card():

                    def submit():
                        controller.send_utc(
                            int(day.value), int(ms_of_day.value), int(sub_ms.value)
                        )
                        dialog_send_utc.close()

                    ui.label("Send UTC")
                    day = ui.number(label="Day", value=0, format="%1d")
                    ms_of_day = ui.number(label="ms of day", value=0, format="%1d")
                    sub_ms = ui.number(label="sub ms", value=0, format="%1d")
                    with ui.row():
                        ui.button("Cancel", on_click=dialog_send_utc.close)
                        ui.button("Submit", on_click=submit)
                ui.button("Send UTC", on_click=dialog_send_utc.open)

            ##################################################################
            # ui.separator()

            ui.markdown("#### Service 3 (Housekeeping Service)")

            with ui.row():

                with ui.dialog().props("persistent") as dialog_send_3_5, ui.card():

                    def submit():
                        report_ids_ = eval(report_ids.value)
                        if not isinstance(report_ids_, list):
                            report_ids_ = list(report_ids_)
                        services.housekeeping.send_enable_period_housekeeping_reports(
                            1, report_ids_)
                        dialog_send_3_5.close()

                    ui.label("Enable HK Reports")
                    report_ids = ui.input(label="Report IDs", value="")
                    with ui.row():
                        ui.button("Cancel", on_click=dialog_send_3_5.close)
                        ui.button("Submit", on_click=submit)
                ui.button("Enable HK Reports", on_click=dialog_send_3_5.open)

                with ui.dialog().props("persistent") as dialog_send_3_6, ui.card():

                    def submit():
                        report_ids_ = eval(report_ids.value)
                        if not isinstance(report_ids_, list):
                            report_ids_ = list(report_ids_)
                        services.housekeeping.send_disable_period_housekeeping_reports(
                            1, report_ids_)
                        dialog_send_3_6.close()

                    ui.label("Disable HK Reports")
                    report_ids = ui.input(label="Report IDs", value="")
                    with ui.row():
                        ui.button("Cancel", on_click=dialog_send_3_6.close)
                        ui.button("Submit", on_click=submit)
                ui.button("Disable HK Reports", on_click=dialog_send_3_6.open)



            ui.markdown("#### Service 17 (Test Service)")
            with ui.row():

                def send_connection_test():
                    services.test.send_connection_test(responder.node_id)
                    textbox_connection_test.set_content("wait for reply...")

                ui.button(
                    "Connection Test",
                    on_click=send_connection_test,
                )
                textbox_connection_test = ui.markdown("")

            ui.markdown("#### Service 8 (Function Management Service)")

            def perform_function(node_id, function_id, arguments=None):
                if not arguments:
                    services.function_management.send_perform_function(
                        node_id, function_id
                    )
                else:
                    services.function_management.send_perform_function(
                        node_id, function_id, [int(argument.value)]
                    )

            for key, func in services.function_management.function_pool.items():
                node_id, func_id = key
                ui.button(
                    func.function_name,
                    on_click=lambda node_id=node_id, func_id=func_id, arguments=func.arguments: perform_function(
                        node_id, func_id, arguments
                    ),
                )

        with ui.column().style("flex: 1; display: flex; justify-content: flex-end;"):

            ui.markdown("## Monitoring")
            # ui.separator()

            # parameter table
            table = (
                ui.table(columns=columns, rows=rows, row_key="parameter_id")
                .style("width: 100%;")
                .props("dense")
            )

            ui.markdown("## Request Verification Log")
            # ui.separator()

            log_table = (
                ui.table(
                    columns=[
                        {
                            "name": "timestamp",
                            "label": "Timestamp",
                            "field": "timestamp",
                            "align": "left",
                        },
                        {"label": "Node ID", "field": "node_id"},
                        {"label": "Service", "field": "service"},
                        {"label": "Subtype", "field": "subtype"},
                        {"label": "Data", "field": "data"},
                    ],
                    rows=[],
                )
                .style("height: 500px; width: 100%")
                .classes("h-52")
                .props("virtual-scroll")
                .props("dense")
            )

    def received_parameter_report(node_id, report, report_id=None):
        for i, (parameter_id, value) in enumerate(report.items()):
            parameter = services.parameter_management.get_parameter(parameter_id)
            rows[i] = {
                "parameter_id": str(parameter_id),
                "parameter_name": str(parameter.parameter_name),
                "parameter_value": str(value),
                "last_update": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            }
        table.update()

    def received_verification_report(node_id, case, source_packet):
        add_log_entry(node_id, case[0], case[1], str(source_packet))

    def add_log_entry(node_id, service, subtype, data=None):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        log_table.add_row(
            {
                "timestamp": timestamp,
                "node_id": node_id,
                "service": service,
                "subtype": subtype,
                "data": data,
            }
        )

    services.request_verification.received_report = received_verification_report
    services.parameter_management.received_parameter_value_report = (
        received_parameter_report
    )
    services.housekeeping.received_housekeeping_report = received_parameter_report
    services.test.received_connection_test_report = (
        lambda node_id: textbox_connection_test.set_content("OK")
    )

    controller.start()


try:
    app.on_startup(setup)
    ui.run(port=args.port)
except KeyboardInterrupt:
    pass

if controller:
    controller.stop()
    controller.disconnect()
